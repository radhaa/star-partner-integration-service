package com.starhealth.partner.integration.service.model;

public class UserValidateRequest {
	private String userName;
	private String password;
	private String emailId;
	private String customerCode;
	private String branchCode;
	private String spCode;
	private String lgCode;
	private String appRefNumber;
	private String orderNumber;
	private boolean acrApproval;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getSpCode() {
		return spCode;
	}

	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	public String getLgCode() {
		return lgCode;
	}

	public void setLgCode(String lgCode) {
		this.lgCode = lgCode;
	}

	public String getAppRefNumber() {
		return appRefNumber;
	}

	public void setAppRefNumber(String appRefNumber) {
		this.appRefNumber = appRefNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public boolean isAcrApproval() {
		return acrApproval;
	}

	public void setAcrApproval(boolean acrApproval) {
		this.acrApproval = acrApproval;
	}

}
