package com.starhealth.partner.integration.service.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String partnerId;
	private String intermediaryId;
	private String intermediaryBranchId;
	private String subIntermediaryId;
	@JsonIgnore
	private String apiKey;
	@JsonIgnore
	private String type;
	@JsonIgnore
	private String source;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public JwtRequest() {
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getIntermediaryId() {
		return intermediaryId;
	}

	public void setIntermediaryId(String intermediaryId) {
		this.intermediaryId = intermediaryId;
	}

	public String getIntermediaryBranchId() {
		return intermediaryBranchId;
	}

	public void setIntermediaryBranchId(String intermediaryBranchId) {
		this.intermediaryBranchId = intermediaryBranchId;
	}

	public String getSubIntermediaryId() {
		return subIntermediaryId;
	}

	public void setSubIntermediaryId(String subIntermediaryId) {
		this.subIntermediaryId = subIntermediaryId;
	}
}
