package com.starhealth.partner.integration.service.model;

import java.util.Date;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.starhealth.partner.integration.service.config.JwtUtil;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {

	@Autowired
	JwtUtil jwtUtil;

	@Value("${jwt.secretkey}")
	private String secretKey;

	public String getJWTToken(Map<String, Object> claims, String subject) {
		String token = Jwts.builder().setSubject(subject).setClaims(claims)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 6000000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
		return token;
	}

	public JSONObject validateToken(String token, String apikey) {
		try {
			if (jwtUtil.validateToken(token)) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Token active");
				String payload = jwtUtil.getClaims(token);
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(payload);
				json.put("message", "Success");
				json.put("apiKey", apikey);
				json.put("requestId", "");
				return json;
			}
			return failureResponse(apikey);
		} catch (Exception e) {
			return failureResponse(apikey);
		}

	}

	private JSONObject failureResponse(String apikey) {
		JSONObject json = new JSONObject();
		json.put("message", "Token Inactive");
		json.put("apiKey", apikey);
		json.put("requestId", "");
		json.put("partnerId", "");
		json.put("intermediaryId", "");
		json.put("intermediaryBranchId", "");
		json.put("subIntermediaryId", "");
		return json;
	}

}