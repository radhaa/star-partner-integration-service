package com.starhealth.partner.integration.service.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.starhealth.partner.integration.service.model.UserValidateRequest;
import com.starhealth.partner.integration.service.model.UserValidateResponse;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/shpartnerintegration/api/v1/partner-integration-service")
@CrossOrigin(origins = "*")
public class ValidateController {
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = UserValidateResponse.class, message = "Validated successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PostMapping(value = "/validateUser")
	public ResponseEntity<UserValidateResponse> createService(@RequestBody UserValidateRequest createIssueRequest,
			@RequestHeader("token") String token) {
		return null;
	}

}
