package com.starhealth.partner.integration.service.controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.starhealth.partner.integration.service.command.TokenGenerationCommand;
import com.starhealth.partner.integration.service.command.ValidateTokenCommand;
import com.starhealth.partner.integration.service.model.JwtRequest;
import com.starhealth.partner.integration.service.model.JwtResponse;
import com.starhealth.partner.integration.service.model.ValidateTokenRequest;

@RestController
@CrossOrigin
@RequestMapping(value = "/shpartnerintegration/api/v1")
public class JwtAuthenticationController {
	@Autowired
	TokenGenerationCommand tokenGenerationCommand;

	@Autowired
	ValidateTokenCommand validateTokenCommand;

	@RequestMapping(value = "/generatetoken", method = RequestMethod.POST)
	public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestHeader String apiKey,
			@RequestHeader String type, @RequestHeader String source, @RequestBody JwtRequest authenticationRequest)
			throws Exception {
		authenticationRequest.setSource(source);
		authenticationRequest.setApiKey(apiKey);
		authenticationRequest.setType(type);
		return ResponseEntity.ok(tokenGenerationCommand.excute(authenticationRequest));
	}

	@RequestMapping(value = "/validatetoken", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> validateToken(@RequestHeader String token, @RequestHeader String apiKey)
			throws Exception {
		ValidateTokenRequest validateTokenRequest = new ValidateTokenRequest();
		validateTokenRequest.setApiKey(apiKey);
		validateTokenRequest.setToken(token);
		return ResponseEntity.ok(validateTokenCommand.excute(validateTokenRequest));
	}

}