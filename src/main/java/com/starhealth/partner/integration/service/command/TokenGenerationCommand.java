package com.starhealth.partner.integration.service.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starhealth.partner.integration.service.model.JwtRequest;
import com.starhealth.partner.integration.service.model.JwtResponse;
import com.starhealth.partner.integration.service.service.JwtTokenGenerationService;
@Service
public class TokenGenerationCommand implements Command<JwtRequest, JwtResponse> {
	@Autowired
	JwtTokenGenerationService jwtTokenGenerationService;
	String type1 = "BROKER,SP,SPO,AV,BQP,SA,PE";
	String type2 = "SP,SPO,AV,BQP,SA,PE";
	String type3 = "SP,AV,BQP,SA,PE";
	String type4 = "PE";

	@Override
	public JwtResponse excute(JwtRequest request) {
		validateRequest(request);
		JwtResponse response = jwtTokenGenerationService.getTokenResponse(request);
		return response;
	}

	private void validateRequest(JwtRequest request) {
		String type = request.getType();
		if (checkNullOrEmpty(request.getType())) {
			throw new NullPointerException("Type cannot be null or empty");
		}
		if (checkNullOrEmpty(request.getSource())) {
			throw new NullPointerException("Source cannot be null or empty");
		}
		if (checkNullOrEmpty(request.getApiKey())) {
			throw new NullPointerException("Api key cannot be null or empty");
		}
		if (type1.contains(type) && (checkNullOrEmpty(request.getPartnerId()))) {
			throw new NullPointerException("For " + request.getType() + " type, partnerId can not be null or empty");
		}
		if (type2.contains(type) && (checkNullOrEmpty(request.getIntermediaryId()))) {
			throw new NullPointerException(
					"For " + request.getType() + " type, IntermediaryId can not be null or empty");
		}
		if (type3.contains(type) && (checkNullOrEmpty(request.getIntermediaryBranchId()))) {
			throw new NullPointerException(
					"For " + request.getType() + " type, IntermediaryBranchId can not be null or empty");

		}
		if (type4.contains(type) && (checkNullOrEmpty(request.getSubIntermediaryId()))) {
			throw new NullPointerException(
					"For " + request.getType() + " type, subIntermediaryId can not be null or empty");

		}

	}

	public boolean checkNullOrEmpty(String string) {
		if (string == null || string.trim().isEmpty())
			return true;
		return false;

	}

}
