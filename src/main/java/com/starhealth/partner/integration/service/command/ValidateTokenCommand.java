package com.starhealth.partner.integration.service.command;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.starhealth.partner.integration.service.model.ValidateTokenRequest;
import com.starhealth.partner.integration.service.service.TokenAuthenticationService;

@Component
public class ValidateTokenCommand implements Command<ValidateTokenRequest, ResponseEntity<JSONObject>> {

	@Autowired
	TokenAuthenticationService tokenAuthenticationService;

	@Override
	public ResponseEntity<JSONObject> excute(ValidateTokenRequest validateTokenRequest) {
		JSONObject response = tokenAuthenticationService.validateToken(validateTokenRequest.getToken(),
				validateTokenRequest.getApiKey());
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}
