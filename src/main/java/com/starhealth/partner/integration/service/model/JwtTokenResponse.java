package com.starhealth.partner.integration.service.model;

public class JwtTokenResponse extends JwtResponse {
	private String redirectionUrl;

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

}
