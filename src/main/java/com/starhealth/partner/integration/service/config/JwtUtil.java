package com.starhealth.partner.integration.service.config;

import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Function;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtUtil {
	
	@Value("${jwt.secretkey}")
	private String secretKey;

	public boolean validateToken(String jwt) {
		if (jwt != null && jwt.startsWith("Bearer ")) {
			jwt = jwt.substring(7);
		}
		return (!isTokenExpired(jwt));
	}

	private boolean isTokenExpired(String jwt) {
		Date date = extractExpiration(jwt);
		return date.before(new Date(System.currentTimeMillis()));
	}

	private Date extractExpiration(String jwt) {
		return extractClaim(jwt, Claims::getExpiration);
	}

	private <T> T extractClaim(String jwt, Function<Claims, T> claimResolver) {
		final Claims claims = extractAllClaims(jwt);
		return claimResolver.apply(claims);
	}

	private Claims extractAllClaims(String jwt) {
		return Jwts.parser().setSigningKey(secretKey.getBytes()).parseClaimsJws(jwt).getBody();
	}

	public String getClaims(String token) {
		String[] split_string = token.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64Url = new Base64();
		String body = new String(base64Url.decode(base64EncodedBody));
		return body;
	}

}