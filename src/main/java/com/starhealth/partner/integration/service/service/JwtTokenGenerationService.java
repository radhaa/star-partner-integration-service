package com.starhealth.partner.integration.service.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starhealth.partner.integration.service.model.JwtRequest;
import com.starhealth.partner.integration.service.model.JwtResponse;
import com.starhealth.partner.integration.service.model.JwtTokenResponse;
import com.starhealth.partner.integration.service.model.TokenAuthenticationService;

@Service
public class JwtTokenGenerationService {
	String reDirectUrl = "https://vo-uat.starhealth.in/partner-landing?";
	@Autowired
	TokenAuthenticationService authenticationService;
	String type1 = "BROKER,SP,SPO,AV,BQP,SA,PE";
	String type2 = "SP,SPO,AV,BQP,SA,PE";
	String type3 = "SP,AV,BQP,SA,PE";
	String type4 = "PE";

	public JwtResponse getTokenResponse(JwtRequest request) {
		Map<String, Object> claims = getToKenCreateContent(request);
		String token = authenticationService.getJWTToken(claims, request.getApiKey());
		JwtTokenResponse jwtResponse = new JwtTokenResponse();
		jwtResponse.setMessage("Success");
		String redirectUrl = reDirectUrl + "type=" + request.getType() + "&token=" + token + "&source="
				+ request.getSource();
		jwtResponse.setRedirectionUrl(redirectUrl);
		jwtResponse.setToken(token);
		return jwtResponse;
	}

	private Map<String, Object> getToKenCreateContent(JwtRequest request) {
		Map<String, Object> claims = new HashMap<>();
		String type = request.getType();
		if (type1.contains(type))
			claims.put("partnerId", request.getPartnerId());
		if (type2.contains(type))
			claims.put("intermediaryId", request.getIntermediaryId());
		if (type3.contains(type))
			claims.put("intermediaryBranchId", request.getIntermediaryBranchId());
		if (type4.contains(type))
			claims.put("subIntermediaryId", request.getSubIntermediaryId());
		return claims;

	}

}
