package com.starhealth.partner.integration.service.command;

import org.springframework.stereotype.Service;

import com.starhealth.partner.integration.service.model.UserValidateRequest;
import com.starhealth.partner.integration.service.model.UserValidateResponse;

@Service
public class UserValidateCommand implements Command<UserValidateRequest, UserValidateResponse>{

	@Override
	public UserValidateResponse excute(UserValidateRequest request) {
		return null;
	}

}
