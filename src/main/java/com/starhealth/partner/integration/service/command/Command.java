package com.starhealth.partner.integration.service.command;

public interface Command<E, T> {

	public T excute(E request);

}
